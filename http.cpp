#include <iostream>
#include <fstream>
#include <wait.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sstream>
using namespace std;

void handle(int newsockfd)
{
    char buffer[5001];
    while( read(newsockfd, buffer, 5000) > 0 )
    {
        stringstream ss((string)buffer);
        string request, method, path, protocol;
        ss >> method >> path >> protocol;
        path.erase(0, 1);
        if(path.find(".cgi") == string::npos)
        {
            char file[5000];
            bzero(file, sizeof(file));
            ifstream fin(path.c_str());
            fin.read(file, sizeof(file));
            write(newsockfd, "HTTP/1.0 200 OK\n\n", 17);
            write(newsockfd, file, sizeof(file));
        }
        else
        {
            stringstream sss(path);
            string cgi, query;
            getline(sss, cgi, '?');
            getline(sss, query);
            write(newsockfd, "HTTP/1.0 200 OK\n", 17);

            setenv("QUERY_STRING", query.c_str(), true);
            setenv("CONTENT_LENGTH", "0", true);
            setenv("REQUEST_METHOD", "GET", true);
            setenv("SCRIPT_NAME", "name", true);
            setenv("REMOTE_HOST", "0.0.0.0", true);
            setenv("REMOTE_ADDR", "0.0.0.0", true);
            setenv("AUTH_TYPE", "ty your mom", true);
            setenv("REMOTE_USER", "motherfucker", true);
            setenv("REMOTE_IDENT", "yeeeeeeeee", true);

            dup2(newsockfd, STDOUT_FILENO);
            execl(cgi.c_str(), NULL);
        }
    }
}
void child(int handle)
{
    wait(nullptr);
}
void term(int handle)
{
    exit(0);
}
int main(int argc, char *argv[])
{
    signal(SIGINT, term);
    signal(SIGTERM, term);
    signal(SIGCHLD, child);
     int sockfd, newsockfd, portno;
     socklen_t clilen;
     char buffer[256];
     struct sockaddr_in serv_addr, cli_addr;
     int n;
     if (argc < 2)
     {
         perror("ERROR, no port provided");
         exit(1);
     }

     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) perror("ERROR opening socket");

     bzero((char *) &serv_addr, sizeof(serv_addr));
     portno = atoi(argv[1]);
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
     if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) perror("ERROR on binding");
     listen(sockfd,5);

    while(1)
    {

        clilen = sizeof(cli_addr);
        newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
        if (newsockfd < 0) continue;

        int pid = fork();
        if (pid  < 0) perror("fork failed");
        else if (pid == 0)
        {
            handle(newsockfd);
            close(sockfd);
            close(newsockfd);
            exit(0);
        }
        else close(newsockfd);
    }
}
