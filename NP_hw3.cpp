#include <windows.h>
#include <list>
#include <string>
#include <string.h>
#include <sstream>
#include <fstream>
#include <stdlib.h>
#include <vector>
#include <map>
using namespace std;

#include "resource.h"

#define SERVER_PORT 7799


#define WM_SOCKET_NOTIFY (WM_USER + 1)
#define WM_SERVER_NOTIFY (WM_USER + 2)
#define F_CONNECTING 0
#define F_READING 1
#define F_WRITING 2
#define F_DONE 3

struct d
{
	int fd;
	int status;
	int mID;
	string IP;
	string port;
	string lastStr;
	fstream file;
};
map<int, int> qq; // sockfd -> info index
vector <string> output(1);
vector<d> info(5);
int conn = 0;
int browser;
map<int, string> unsend;
void change(string *linetmp)
{
	for (int i = 0; i < linetmp->size(); ++i)
	{
		if ((*linetmp)[i] == '<') linetmp->replace(i, 1, "&lt");
		else if ((*linetmp)[i] == '>') linetmp->replace(i, 1, "&gt");
		else if ((*linetmp)[i] == '"') linetmp->replace(i, 1, "&quot");
		else if ((*linetmp)[i] == '&') linetmp->replace(i, 1, "&amp");
		else if ((*linetmp)[i] == '\'') linetmp->replace(i, 1, "&#39");
		else if ((*linetmp)[i] == '\r') linetmp->erase(i--, 1);
		else if ((*linetmp)[i] == '\n') linetmp->erase(i--, 1);
	}
}
void display(int i, string text, WPARAM ssock, HWND hwnd)
{
	//if (output[output.size() - 1].empty())
	{
		output[output.size() - 1] += "<script>document.all['m";
		output[output.size() - 1] += to_string(i);
		output[output.size() - 1] += "'].innerHTML += \"";
	}
	output[output.size() - 1] += text;
	//if (text != "% ")
	{
		output[output.size() - 1] += "<br>\";</script>\n";
		output.push_back("");
	}
}
BOOL CALLBACK MainDlgProc(HWND, UINT, WPARAM, LPARAM);
int EditPrintf (HWND, TCHAR *, ...);
list<SOCKET> Socks;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow)
{
	
	return DialogBox(hInstance, MAKEINTRESOURCE(ID_MAIN), NULL, MainDlgProc);
}

BOOL CALLBACK MainDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	WSADATA wsaData;

	static HWND hwndEdit;
	static SOCKET msock, ssock;
	static struct sockaddr_in sa;

	int err;

	int error = 0;
	int error_len = sizeof(error);
	int index = 0;
	char buffer[5001];
	char file[5001];
	char message[5001];
	stringstream ss, sss, parse;
	string request, method, path, protocol, cgi, query, part, tmp, filename, linetmp, command;
	switch(Message) 
	{
		case WM_INITDIALOG:
			hwndEdit = GetDlgItem(hwnd, IDC_RESULT);
			break;
		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case ID_LISTEN:

					WSAStartup(MAKEWORD(2, 0), &wsaData);

					//create master socket
					msock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

					if( msock == INVALID_SOCKET ) {
						EditPrintf(hwndEdit, TEXT("=== Error: create socket error ===\r\n"));
						WSACleanup();
						return TRUE;
					}

					err = WSAAsyncSelect(msock, hwnd, WM_SOCKET_NOTIFY, FD_ACCEPT | FD_CLOSE | FD_READ | FD_WRITE);

					if ( err == SOCKET_ERROR ) {
						EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
						closesocket(msock);
						WSACleanup();
						return TRUE;
					}

					//fill the address info about server
					sa.sin_family		= AF_INET;
					sa.sin_port			= htons(SERVER_PORT);
					sa.sin_addr.s_addr	= INADDR_ANY;

					//bind socket
					err = bind(msock, (LPSOCKADDR)&sa, sizeof(struct sockaddr));

					if( err == SOCKET_ERROR ) {
						EditPrintf(hwndEdit, TEXT("=== Error: binding error ===\r\n"));
						WSACleanup();
						return FALSE;
					}

					err = listen(msock, 2);
		
					if( err == SOCKET_ERROR ) {
						EditPrintf(hwndEdit, TEXT("=== Error: listen error ===\r\n"));
						WSACleanup();
						return FALSE;
					}
					else {
						EditPrintf(hwndEdit, TEXT("=== Server START ===\r\n"));
					}

					break;
				case ID_EXIT:
					EndDialog(hwnd, 0);
					break;
			};
			break;

		case WM_CLOSE:
			EndDialog(hwnd, 0);
			break;

		case WM_SOCKET_NOTIFY:
			switch( WSAGETSELECTEVENT(lParam) )
			{
				case FD_ACCEPT:
					ssock = accept(msock, NULL, NULL);
					Socks.push_back(ssock);
					//WSAAsyncSelect(msock, hwnd, WM_SOCKET_NOTIFY, FD_CLOSE | FD_READ | FD_WRITE);
					EditPrintf(hwndEdit, TEXT("=== Accept one new client(%d), List size:%d ===\r\n"), ssock, Socks.size());

					break;
				case FD_READ:
				//Write your code for read event here.
					EditPrintf(hwndEdit, TEXT("=== WM_SOCKET_NOTIFY Sock #%d FD_READ ===\r\n"), wParam);
					memset(buffer, 0, sizeof(buffer));
					if (recv(wParam, buffer, sizeof(buffer), 0) > 0)
					{
						//EditPrintf(hwndEdit, TEXT(buffer), wParam);
						ss << (string)buffer;
						ss >> method >> path >> protocol;
						path.erase(0, 1);
						if (path.find(".cgi") == string::npos)
						{
							memset(file, 0, sizeof(file));
							ifstream fin(path.c_str());
							fin.read(file, sizeof(file));
							send(wParam, "HTTP/1.0 200 OK\n\n", 17, 0);
							int len = send(wParam, file, sizeof(file), 0);
							if(len == sizeof(file) ) closesocket(wParam);
							else
							{
								if(unsend[wParam].empty()) unsend.insert(pair<int, string>(wParam, ((string)file).substr(len)));
								else unsend[wParam] += ((string)file).substr(len);
							}

						}
						else
						{
							browser = ssock;
							sss << path;
							getline(sss, cgi, '?');
							getline(sss, query);
							send(wParam, "HTTP/1.0 200 OK\n", 17, 0);

							if (cgi == "hw3.cgi")
							{
								parse << query;
								while (getline(parse, part, '&'))
								{
									if (part[0] == 'h') info[index].IP = part.substr(3);
									if (part[0] == 'p') info[index].port = part.substr(3);
									if (part[0] == 'f')
									{
										filename = part.substr(3);
										info[index].file.open(filename.c_str(), ios::in);
										++index;
									}
								}

								struct sockaddr_in serv_addr[5];
								for (int i = 0; i < 5; ++i)
								{
									if (info[i].IP == "" || info[i].port == "")
									{
										info[i].fd = -1;
										info[i].mID = -1;
										continue;
									}

									info[i].mID = conn++;
									info[i].fd = socket(AF_INET, SOCK_STREAM, 0);
									WSAAsyncSelect(info[i].fd, hwnd, WM_SERVER_NOTIFY, FD_CONNECT | FD_CLOSE);
									EditPrintf(hwndEdit, TEXT( (TCHAR*) to_string(conn).c_str()), wParam);
									qq.insert( pair<int, int>(info[i].fd, i) );
									memset(&serv_addr[i], 0, sizeof(serv_addr[i]));
									serv_addr[i].sin_family = AF_INET;
									serv_addr[i].sin_addr = *((struct in_addr *) gethostbyname(info[i].IP.c_str())->h_addr);
									serv_addr[i].sin_port = htons(atoi(info[i].port.c_str()));
								}

								send(wParam, "Content-type: text/html\n\n<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" /><title>Network Programming Homework 3</title>", 151, 0);
								send(wParam, "</head><body bgcolor=#336699><font face=\"Courier New\" size=2 color=#FFFF99><table width=\"1000\" border=\"1\"><tr>", 111, 0);

								for (int i = 0; i < 5; ++i)
									if (info[i].mID == -1);
									else
									{
										stringstream tt;
										tt << "<td valign=\"top\" id =\"m" << i << "\"></td>";
										EditPrintf(hwndEdit, TEXT((TCHAR*)tt.str().c_str()), wParam);
										send(wParam, tt.str().c_str(), tt.str().size(), 0);
									}
								send(wParam, "</tr></table>", 14, 0);
				
								for (int i = 0; i < 5; i++)
								{
									if (info[i].fd != -1)
									{
										connect(info[i].fd, (struct sockaddr *)&serv_addr[i], sizeof(serv_addr[i]));
									}
								}
							}
						}
					}
					WSAAsyncSelect(ssock, hwnd, WM_SOCKET_NOTIFY, FD_CLOSE);				
					break;

				case FD_WRITE:
				//Write your code for write event here
					EditPrintf(hwndEdit, TEXT("=== WM_SOCKET_NOTIFY Sock #%d FD_WRITE ===\r\n"), wParam);
					if (!unsend[wParam].empty())
					{
						int len;
						len = send(wParam, unsend[wParam].c_str(), unsend[wParam].size(), 0);
						if (len != unsend[wParam].size()) unsend[wParam] = unsend[wParam].substr(len);
					}
					for (auto &it : output)
					{
						//EditPrintf(hwndEdit, TEXT((TCHAR*)it.c_str()), wParam);
						int len = send(wParam, it.c_str(), it.size(), 0);
						if (len != it.size()) unsend[wParam] += it.substr(len);
					}
					output.clear();
					output.resize(1);

					break;
				case FD_CLOSE:
					break;
			};
			break;
		case WM_SERVER_NOTIFY:
			switch (WSAGETSELECTEVENT(lParam))
			{
				case FD_CONNECT:
					EditPrintf(hwndEdit, TEXT("=== WM_SERVER_NOTIFY Sock #%d FD_CONNECT ===\r\n"), wParam);
					if (getsockopt(wParam, SOL_SOCKET, SO_ERROR, (char*)&error, &error_len) == SOCKET_ERROR || error != 0) 
					{
						break;
					}
					WSAAsyncSelect(wParam, hwnd, WM_SERVER_NOTIFY, FD_READ | FD_WRITE | FD_CLOSE);
					info[qq[wParam]].status = F_READING;
					break;

				case FD_READ:
					if (info[qq[wParam]].status == F_WRITING) break;
					EditPrintf(hwndEdit, TEXT("=== WM_SERVER_NOTIFY Sock #%d FD_READ ===\r\n"), wParam);
					memset(buffer, 0, 5001 * sizeof(char));

					if (recv(wParam, buffer, 5000, 0) > 0)
					{
						//EditPrintf(hwndEdit, TEXT(buffer), wParam);

						if (!info[qq[wParam]].lastStr.empty())
						{
							linetmp = info[qq[wParam]].lastStr;
							info[qq[wParam]].lastStr.clear();
						}
						linetmp.clear();
						for (int j = 0; buffer[j] != '\0'; ++j)
						{
							linetmp.push_back(buffer[j]);
							if (buffer[j] == '\n')
							{
								if (linetmp[0] == '%' && linetmp[1] == ' ')
								{
									info[qq[wParam]].status = F_WRITING;
									break;
								}
								change(&linetmp);
								display(info[qq[wParam]].mID, linetmp, ssock, hwnd);
								linetmp.clear();
							}
						}
						if (!linetmp.empty())
						{
							if (linetmp[0] == '%')
							{
								info[qq[wParam]].status = F_WRITING;
								//change(&linetmp);
								//display(info[qq[wParam]].mID, linetmp, ssock, hwnd);
							}
							else info[qq[wParam]].lastStr = linetmp;
						}
						WSAAsyncSelect(ssock, hwnd, WM_SOCKET_NOTIFY, FD_WRITE | FD_CLOSE);

						if (info[qq[wParam]].status == F_DONE)
						{
							WSAAsyncSelect(wParam, hwnd, WM_SERVER_NOTIFY, FD_CLOSE);
						}
						else if (info[qq[wParam]].status == F_WRITING) WSAAsyncSelect(wParam, hwnd, WM_SERVER_NOTIFY, FD_WRITE | FD_CLOSE);
						else WSAAsyncSelect(wParam, hwnd, WM_SERVER_NOTIFY, FD_READ | FD_CLOSE);
					}

					break;
				case FD_WRITE:
					if (info[qq[wParam]].status != F_WRITING) break;
					EditPrintf(hwndEdit, TEXT("=== WM_SERVER_NOTIFY Sock #%d FD_WRITE ===\r\n"), wParam);
					//EditPrintf(hwndEdit, TEXT( (TCHAR*) to_string(info[qq[wParam]].fd).c_str()), wParam);
					getline(info[qq[wParam]].file, tmp);

					tmp += "\n";
					send(wParam, tmp.c_str(), tmp.size(), 0);
					tmp.pop_back();

					change(&command);
					command = (string)"% " + (string)"<b>" + tmp + (string)"</b>";
					display(info[qq[wParam]].mID, command, ssock, hwnd);
					WSAAsyncSelect(browser, hwnd, WM_SOCKET_NOTIFY, FD_WRITE | FD_CLOSE);
					//EditPrintf(hwndEdit, TEXT((TCHAR*)to_string(ssock).c_str()), wParam);

					if (tmp == "exit") info[qq[wParam]].status = F_DONE;
					else info[qq[wParam]].status = F_READING;
					WSAAsyncSelect(wParam, hwnd, WM_SERVER_NOTIFY, FD_READ | FD_CLOSE);
					break;

				case FD_CLOSE:
					if (info[qq[wParam]].fd == -1)break;
					EditPrintf(hwndEdit, TEXT("=== WM_SERVER_NOTIFY Sock #%d FD_CLOSE ===\r\n"), wParam);
					conn--;
					//EditPrintf(hwndEdit, TEXT((TCHAR*)to_string(ssock).c_str()), wParam);
					if (conn == 0)
					{
						qq.clear();
						info.clear();
						info.resize(5);
						output.push_back("</font></body></html>");
						WSAAsyncSelect(browser, hwnd, WM_SOCKET_NOTIFY, FD_WRITE | FD_CLOSE);
					}
					closesocket(wParam);
					info[qq[wParam]].fd = -1;
					break;

			};
			break;
		default:
			return FALSE;


	};

	return TRUE;
}
int EditPrintf (HWND hwndEdit, TCHAR * szFormat, ...)
{
     TCHAR   szBuffer [1024] ;
     va_list pArgList ;

     va_start (pArgList, szFormat) ;
     wvsprintf (szBuffer, szFormat, pArgList) ;
     va_end (pArgList) ;

     SendMessage (hwndEdit, EM_SETSEL, (WPARAM) -1, (LPARAM) -1) ;
     SendMessage (hwndEdit, EM_REPLACESEL, FALSE, (LPARAM) szBuffer) ;
     SendMessage (hwndEdit, EM_SCROLLCARET, 0, 0) ;
	 return SendMessage(hwndEdit, EM_GETLINECOUNT, 0, 0); 
}